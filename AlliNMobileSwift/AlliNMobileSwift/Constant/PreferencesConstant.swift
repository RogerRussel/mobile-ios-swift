//
//  PreferencesConstant.swift
//  AlliNMobileSwift
//
//  Created by Lucas Rodrigues on 05/06/17.
//  Copyright © 2017 Lucas Rodrigues. All rights reserved.
//
struct PreferencesConstant {
    static let KEY_APPVERSION = "allin_key_appversion";
    static let KEY_DEVICE_ID = "allin_key_device_id";
    static let KEY_USER_EMAIL = "allin_key_user_email";
    static let KEY_PROJECT_ID = "allin_key_project_id";
    static let PREFERENCES_ID = "allin_notifications";
}
