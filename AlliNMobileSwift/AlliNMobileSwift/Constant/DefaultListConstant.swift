//
//  DefaultListConstant.swift
//  AlliNMobileSwift
//
//  Created by Lucas Rodrigues on 05/06/17.
//  Copyright © 2017 Lucas Rodrigues. All rights reserved.
//
struct DefaultListConstant {
    static let LISTA_PADRAO = "Lista Padrao Push";
    static let ID_PUSH = "id_push";
    static let PUSH_ID = "push_id";
    static let PLATAFORMA = "plataforma";
}
