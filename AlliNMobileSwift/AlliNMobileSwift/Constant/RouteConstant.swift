//
//  RouteConstant.swift
//  AlliNMobileSwift
//
//  Created by Lucas Rodrigues on 05/06/17.
//  Copyright © 2017 Lucas Rodrigues. All rights reserved.
//
struct RouteConstant {
    static let EMAIL = "/email";
    static let ADD_LIST = "/addlist";
    static let DEVICE = "/device";
    static let DEVICE_LOGOUT = "/device/logout";
    static let DEVICE_DISABLE = "/device/disable";
    static let DEVICE_ENABLE = "/device/enable";
    static let DEVICE_STATUS = "/device/status";
    static let CAMPAIGN = "/campanha";
    static let NOTIFICATION_CAMPAIGN = "/notification/campaign";
    static let NOTIFICATION_TRANSACTIONAL = "/notification/transactional";
    static let UPDATE = "update";
}
